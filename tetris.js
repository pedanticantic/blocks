var tetris = {
    areaWidth: 15,
    areaHeight: 22,
    gridsize: 10,
    allSquares: [],
    allShapes: [],
    nextSquareId: 0,
    score: 0,
    repeat: null,
    repeatInterval: 500,
    cycles: 0,  // Determines when the game speeds up.
    // One-time initialisation. Basically sets up event handlers, etc.
    init: function() {
        $('body').on('keydown', function(ev) {
            tetris.handleUserInput(ev.which);
        });
        $('#controls button').on('click', function() {
            tetris.handleUserInput($(this).data('tetrisKeyCode'));
        });
        $('body').bind('swipeone', tetris.handleSwipe);
        $('#start').on('click', tetris.startGame);
        $('#quit').on('click', tetris.gameOver);
    },
    startGame: function() {
        $('#start').removeClass('stopped');
        $('#quit').removeClass('stopped');
        window.setTimeout(function() {
            $('#start').addClass('in_progress')
            $('#quit').addClass('in_progress')
        }, 1);
        $('#game_over').removeClass('swoop');
        tetris.incScore(-1);  // Has the effect of resetting the score.
        
        // Remove any squares from the last game.
        for(squareIdx in tetris.allSquares) {
            tetris.allSquares[squareIdx].hideSquare();
        }
        tetris.allSquares = [];
        
        // Set the size of the play area.
        var widthBucket = parseInt($('#width_setting').val());
        widthBucket = (widthBucket >= 0 && widthBucket <= 2) ? widthBucket : 0; // In case people have been messing.
        widthBucket = (7 + 4 * widthBucket);
        tetris.areaWidth = widthBucket;
        widthBucket = (widthBucket * 10) + 'px';
        var heightBucket = parseInt($('#height_setting').val());
        heightBucket = (heightBucket >= 0 && heightBucket <= 2) ? heightBucket : 0; // In  + 'px'case people have been messing.
        heightBucket = (12 + 5 * heightBucket);
        tetris.areaHeight = heightBucket;
        heightBucket = (heightBucket * 10);
        topBucket = (270 - heightBucket) + 'px';
        heightBucket += 'px';
        $('#main_bucket').css({top: topBucket, width: widthBucket, height: heightBucket});
        
        var shape1 = new shape(null, 5, tetris.areaHeight - 2);
    
        tetris.repeat = window.setInterval(tetris.doMove, tetris.repeatInterval);
    },
    gameOver: function() {
        window.clearInterval(tetris.repeat);
        $('#game_over').addClass('swoop');
        $('#start').removeClass('in_progress');
        $('#quit').removeClass('in_progress');
        window.setTimeout(function() {
            $('#start').addClass('stopped');
            $('#quit').addClass('stopped');
        }, 10);
        for(shapeIndex in tetris.allShapes) {
            tetris.allShapes[shapeIndex].stopSquares();  // Tells all the squares they're no longer in a shape.
        }
        tetris.allShapes = [];  // Clears all references to the shapes.
    },
    // Method to interpret a swipe into a direction.
    handleSwipe: function(dummy, ev) {
        // Get the actual swipe vector.
        dx = ev.delta[0].startX;
        dy = ev.delta[0].startY;
        // Determine whether it was more horizontal or more vertical.
        if (Math.abs(dx) > Math.abs(dy)) {
            // More horizontal.
            if (dx > 0) {
                // To the right.
                effCode = 39;
            } else {
                // To the left.
                effCode = 37;
            }
        } else {
            // More vertical.
            if (dy > 0) {
                // Down.
                effCode = 40;
            } else {
                // Up.
                effCode = 38;
            }
        }
        tetris.handleUserInput(effCode);
    },
    // Handle user input. Can be called from keydown handler, or from button click handler.
    handleUserInput: function(keyCode) {
        switch (keyCode) {
            case 37:  // Left arrow
                tetris.shapeMove('left');
                break;
            case 39:  // Right arrow
                tetris.shapeMove('right');
                break;
            case 38:  // Up arrow
                tetris.shapeRotate();
                break;
            case 40:  // Down arrow
                tetris.shapeDown();
                break;
            case 81:  // User wants to quit
                tetris.gameOver();
                break;
        }
    },
    getNextId: function() {
        return ++this.nextSquareId;
    },
    // Method to move all the shapes either left or right.
    shapeMove: function(dir) {
        moveLeft = (dir == 'left');  // If anything other than 'left' is passed in, right will be assumed.
        for(shapeIndex in tetris.allShapes) {
            tetris.allShapes[shapeIndex].moveShape(moveLeft ? 'left' : 'right');
        }
    },
    shapeRotate: function() {
        for(shapeIndex in tetris.allShapes) {
            tetris.allShapes[shapeIndex].rotateShape();
        }
    },
    shapeDown: function() {
        for(shapeIndex in tetris.allShapes) {
            tetris.allShapes[shapeIndex].moveShape('down');
        }
    },
    // Method to return true if the given shape will collide with anything if its x
    // position is changed by xDelta and its y position is changed by yDelta.
    willCollide: function(shape, xDelta, yDelta, onlyNonShapes) {
        xDelta = (xDelta == undefined ? 0 : parseInt(xDelta, 10));
        yDelta = (yDelta == undefined ? 0 : parseInt(yDelta, 10));
        // Loop through each square in the shape...
        for(squareIdx in shape.items) {
            xpos = shape.items[squareIdx].xpos + xDelta;
            ypos = shape.items[squareIdx].ypos + yDelta;
            if (xpos < 0 || xpos >= tetris.areaWidth) {
                return true;  // It's off the scale!
            }
            if (ypos < 0) {
                return true;  // Again, it's off the scale!
            }
            // See if its new position will clash with any other square not in this shape.
            for(squaresIdx in tetris.allSquares) {
                thisSquare = tetris.allSquares[squaresIdx];
                if (!shape.containsSquare(thisSquare.id)) {
                    if ((onlyNonShapes === true && !thisSquare.inShape) ||
                        (onlyNonShapes === false && thisSquare.inShape) ||
                        onlyNonShapes == undefined) {
                        if (thisSquare.xpos == xpos && thisSquare.ypos == ypos) {
                            // Yes, collision.
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    },
    // Method to check for horizontal lines. If there are any, remove them,
    // move all squares above them down, and increase the score.
    checkHorizontalLines: function() {
        fullLineFactor = 0;
        // Loop down from top to bottom.
        for(vIdx = tetris.areaHeight ; vIdx >= 0 ; vIdx--) {
            // Look across this line and see if there's a non-shape square in every
            // position.
            fullLine = true;
            for(hIdx = 0 ; hIdx < tetris.areaWidth ; hIdx++) {
                foundThisCell = false;
                for(squareIdx in tetris.allSquares) {
                    thisSquare = tetris.allSquares[squareIdx];
                    if (thisSquare.ypos == vIdx && thisSquare.xpos == hIdx && !thisSquare.inShape) {
                        foundThisCell = true;
                    }
                }
                fullLine = fullLine && foundThisCell;
            }
            if (fullLine) {
                fullLineFactor++;
                tetris.incScore(100 * fullLineFactor);
                // The whole line is filled!
                // Loop through all the non-shape squares. If any are on the line, remove them.
                // If any are above the line, drop them down by 1 position.
                // We loop backwards because we'll be removing squares from the array.
                for(squareIdx = tetris.allSquares.length - 1 ; squareIdx >= 0 ; squareIdx--) {
                    thisSquare = tetris.allSquares[squareIdx];
                    if (!thisSquare.inShape) {
                        if (thisSquare.ypos == vIdx) {
                            thisSquare.hideSquare();
                            tetris.allSquares.splice(squareIdx, 1);
                        } else {
                            if (thisSquare.ypos > vIdx) {
                                thisSquare.move();  // Drop it down one line.
                            }
                        }
                    }
                }
            }
        }
    },
    // Increment the score, but multiply by the game difficulty factor.
    incScore: function(delta) {
        if (delta == -1) {
            tetris.score = 0;
        } else {
            factor = ((15 - tetris.areaWidth) / 4) +
                     ((22 - tetris.areaHeight) / 5) +
                     1;
            tetris.score += delta * factor;
        }
        $('#score').html(tetris.score);
    },
    doMove: function() {
        // Tick the score up.
        tetris.incScore(1);
        // We have to loop backwards through the array because we might remove items
        // from it during the loop.
        for(shapeIndex = tetris.allShapes.length - 1 ; shapeIndex >= 0 ; shapeIndex--) {
            hasFinished = tetris.allShapes[shapeIndex].moveShape();
            if (hasFinished) {
                // Tell this shape it is finished.
                tetris.allShapes[shapeIndex].stopSquares();  // Tells all the squares they're no longer in a shape.
                tetris.allShapes.splice(shapeIndex, 1);
                // Ask the game to check whether there are any completed horizontal lines,
                // and deal with them accordingly.
                tetris.checkHorizontalLines();
            }
        }
        // Ocassionally add a shape to the top.
        if (tetris.allShapes.length == 0 || Math.random() > 0.95) {
            shape1 = new shape(null, 1 + Math.floor(Math.random() * (tetris.areaWidth-2)), tetris.areaHeight - 2);
            while(tetris.willCollide(shape1, 0, 0, false)) {
                shape1.moveShape('up');
            }
            // If this new shape is "touching", the game is over. Kill all the shapes!
            if (shape1.touching()) {
                tetris.gameOver();
                return;
            }
        }
        // See if we need to speed up the game.
        tetris.cycles++;
        if (tetris.cycles > 200) {
            tetris.cycles = 0;
            tetris.repeatInterval = Math.floor(tetris.repeatInterval * 0.95);
            window.clearInterval(tetris.repeat);
            tetris.repeat = window.setInterval(tetris.doMove, tetris.repeatInterval);
        }
    }

};

// Class to represent an individual square.
function square(x, y, colour, inShape) {
    this.draw = function() {
        var parentContainer = $(this.domElement).parent();
        var containerHeight = parentContainer.height() - 2;
        $(this.domElement).css({
            left: (this.xpos * tetris.gridsize) + 'px',
            top: (containerHeight - (this.ypos + 1) * tetris.gridsize) + 'px'
        });
    };
    
    this.id = tetris.getNextId();
    this.xpos = x;
    this.ypos = y;
    if (this.ypos === null) {
        this.ypos = tetris.areaHeight;
    }
    this.inShape = (inShape == null ? true : !!inShape);
    this.colour_class = colour;
    this.domElement = $('<div class="square colour_' + this.colour_class + '" ></div>');
    this.domElement.appendTo('#main_bucket');
    
    this.draw();
    tetris.allSquares.push(this);
    
    this.move = function(dir) {
        dir = (dir == undefined ? 'down' : dir);
        switch (dir) {
            case 'down':
                this.ypos--;
                this.draw();
                break;
            case 'left':
                this.xpos--;
                this.draw();
                break;
            case 'right':
                this.xpos++;
                this.draw();
                break;
            case 'up':
                // This is a special case and isn't called very often.
                this.ypos++;
                this.draw();
                break;
        }
    };
    
    this.removeFromShape = function() {
        this.inShape = false;
    }
    
    // We have finished with the square. Remove it from screen.
    this.hideSquare = function() {
        $(this.domElement).remove();
    }
    
}

// Class to represent a group of squares, ie a shape.
// The group type determines which shape the squares are in.
function shape(groupType, x, y) {
    this.items = [];
    if (groupType === null) {
        groupType = Math.floor(7 * Math.random());
    }
    switch (groupType) {
        case 'blob':
        case 0:
            this.items.push(new square(x-1, y+1, 'red'));
            this.items.push(new square(x-1, y, 'red'));
            this.items.push(new square(x, y+1, 'red'));
            this.items.push(new square(x, y, 'red'));
            break;
        case 'line':
        case 1:
            this.items.push(new square(x, y+1, 'yellow'));
            this.items.push(new square(x, y, 'yellow'));
            this.items.push(new square(x, y-1, 'yellow'));
            this.items.push(new square(x, y-2, 'yellow'));
            break;
        case 'el-left':
        case 2:
            this.items.push(new square(x, y+1, 'blue'));
            this.items.push(new square(x, y, 'blue'));
            this.items.push(new square(x, y-1, 'blue'));
            this.items.push(new square(x+1, y-1, 'blue'));
            break;
        case 'el-right':
        case 3:
            this.items.push(new square(x, y+1, 'green'));
            this.items.push(new square(x, y, 'green'));
            this.items.push(new square(x, y-1, 'green'));
            this.items.push(new square(x-1, y-1, 'green'));
            break;
        case 'tee':
        case 4:
            this.items.push(new square(x, y+1, 'orange'));
            this.items.push(new square(x, y, 'orange'));
            this.items.push(new square(x+1, y, 'orange'));
            this.items.push(new square(x, y-1, 'orange'));
            break;
        case 'zig':
        case 5:
            this.items.push(new square(x, y+1, 'pink'));
            this.items.push(new square(x, y, 'pink'));
            this.items.push(new square(x+1, y, 'pink'));
            this.items.push(new square(x+1, y-1, 'pink'));
            break;
        case 'zag':
        case 6:
            this.items.push(new square(x, y+1, 'cyan'));
            this.items.push(new square(x, y, 'cyan'));
            this.items.push(new square(x-1, y, 'cyan'));
            this.items.push(new square(x-1, y-1, 'cyan'));
            break;
    }
    tetris.allShapes.push(this);

    this.moveShape = function(dir) {
        dir = (dir == undefined ? 'down' : dir);
        switch (dir) {
            case 'down':
                // If the shape can move, then move it.
                if (!this.touchingAny()) {
                    for(squareIndex in this.items) {
                        this.items[squareIndex].move();
                    }
                    tetris.incScore(5);  // Only increase the score if the thing actually moved.
                }
                // Return whether it can now move. If it can't, the calling process will deal
                // with it - delete the shape, update scores, etc.
                return this.touching();
                break;
            case 'left':
            case 'right':
                if (!tetris.willCollide(this, (dir == 'left' ? -1 : 1))) {
                    for(squareIndex in this.items) {
                        this.items[squareIndex].move(dir);
                    }
                }
                break;
            // This is a special case and it's called very often (or at least shouldn't be!).
            case 'up':
                for(squareIndex in this.items) {
                    this.items[squareIndex].move('up');
                }
                break;
        }
    }
    
    // Method to rotate this shape. It will always rotate anti-clockwise.
    // We assume it rotates about the 3rd square.
    // We rotate the shape, then test if there's a collision. If there is, we rotate
    // it back again.
    this.rotateShape = function(undo) {
        undo = !!undo;
        rotateIndex = (this.items.length > 1 ? 1 : this.items.length - 1);
        rotateSquare = this.items[rotateIndex];
        for(itemInd in this.items) {
            if (itemInd != rotateIndex) {
                dx = this.items[itemInd].xpos - rotateSquare.xpos;
                dy = this.items[itemInd].ypos - rotateSquare.ypos;
                if (undo) {
                    this.items[itemInd].xpos += -dx + dy;
                    this.items[itemInd].ypos += -dy - dx;
                } else {
                    this.items[itemInd].xpos += -dx -dy;
                    this.items[itemInd].ypos += -dy + dx;
                }
            }
        }
        // Test whether this is now a collision.
        if (!undo && tetris.willCollide(this, 0)) {
            // Undo the rotation!
            this.rotateShape(true);
        }
        this.drawShape();
    }
    
    // This method goes through all the squares in this shape. If any of them are
    // immediately above another square that's not in a shape, we say it's touching.
    this.touching = function() {
        if (tetris.willCollide(this, 0, -1, true)) {
            return true;
        }
        return false;
    }
    
    // This method goes through all the squares in this shape. If any of them are
    // immediately above any other square at all, we say it's touching.
    this.touchingAny = function() {
        if (tetris.willCollide(this, 0, -1)) {
            return true;
        }
        return false;
    }
    
    this.stopSquares = function() {
        for(squareIndex in this.items) {
            this.items[squareIndex].removeFromShape();
        }
    }
    
    this.containsSquare = function(testId) {
        for(squareIndex in this.items) {
            if (this.items[squareIndex].id == testId) {
                return true;
            }
        }
        return false;
    }
    
    this.drawShape = function() {
        for(squareIndex in this.items) {
            this.items[squareIndex].draw();
        }
    }
    
}

$(document).ready(function() {
    tetris.init();
});
